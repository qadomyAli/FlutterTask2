import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'component/OutlineBtnState.dart';
import 'component/RoundedTextField.dart';

// gradient: LinearGradient(
// begin: Alignment.topRight,
// colors: [
// Colors.orange[700],
// Colors.black.withOpacity(0.9),
// ],
// end: Alignment.bottomRight),
class LoginPage extends StatelessWidget {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      // resizeToAvoidBottomPadding: false,
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                  colors: [
                    Colors.orange[700],
                    Colors.black.withOpacity(0.9),
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(1.0, 1.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text("CREATE ACCOUNT",
                      style: TextStyle(color: Colors.white, fontSize: 20)),
                  SizedBox(height: 60),
                  RoundedTextField(hintText: "Name", icon: Icons.menu),
                  RoundedTextField(hintText: "Email", icon: Icons.email),
                  RoundedTextField(hintText: "Password", icon: Icons.lock),
                  SizedBox(height: 25.0),
                  // OutlineBtnState(btnText: "Continue"),
                  FlatButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        Scaffold.of(context).showSnackBar(
                            SnackBar(content: Text('Processing Data')));
                      }
                    },
                    child: OutlineBtnState(btnText: "Continue"),
                  ),
                  Text("Terms & Conditions",
                      style: TextStyle(color: Colors.white.withOpacity(0.2))),
                  SizedBox(height: 25.0),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

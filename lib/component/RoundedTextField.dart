import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'TextFieldContainer.dart';

class RoundedTextField extends StatelessWidget {
  final String hintText;
  final IconData icon;

  const RoundedTextField({Key key, this.hintText, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        style: TextStyle(
          color: Colors.white,
        ),
        decoration: InputDecoration(
          // errorText: "",
          focusedBorder: InputBorder.none,
          icon: Icon(
            icon,
            color: Colors.white.withOpacity(0.4),
          ),
          hintText: hintText,
          hintStyle: TextStyle(
            color: Colors.white.withOpacity(0.5),
          ),
        ),
      ),
    );
  }
}

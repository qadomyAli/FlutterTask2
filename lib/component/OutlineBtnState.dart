import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OutlineBtnState extends StatelessWidget {
  final String btnText;

  OutlineBtnState({this.btnText});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width * 0.9,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.white.withOpacity(0.3),
          width: 1,
        ),
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(30),
      ),
      padding: EdgeInsets.all(20),
      child: Center(
        child: Text(
          btnText,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
